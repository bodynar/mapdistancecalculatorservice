﻿using MAS.Stuff.MapDistanceCalculatorService.Services;
using System;
using MAS.Stuff.MapDistanceCalculatorService.Models;

namespace MAS.Stuff.MapDistanceCalculatorService.Tests.Mocks
{
    public class WebMapResponseParserMock : IWebMapResponseParser
    {
        public DistanceInfo Parse(string response)
        {
            return (DistanceInfo)Activator.CreateInstance(typeof(DistanceInfo));
        }
    }
}
