﻿using MAS.Stuff.MapDistanceCalculatorService.Services;
using System;
using System.Threading.Tasks;
using MAS.Stuff.MapDistanceCalculatorService.Models;

namespace MAS.Stuff.MapDistanceCalculatorService.Tests.Mocks
{
    public class AsyncMapServiceMock : IAsyncMapService
    {
        private readonly DistanceInfo _distanceData;

        public AsyncMapServiceMock()
        {
            _distanceData = new DistanceInfo("OriginPoint", "DestinationPoint", 0, 0);
        }

        public async Task<DistanceInfo> CalculateDistanceBetweenTwoPoints(string origin, string destination)
        {
            await Task.Factory.StartNew(() => { });
            return  _distanceData;
        }

        public async Task<DistanceInfo> CalculateDistanceBetweenTwoPoints(string x1, string y1, string x2, string y2)
        {
            await Task.Factory.StartNew(() => { });
            return _distanceData;
        }
    }
}
