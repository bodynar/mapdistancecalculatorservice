﻿using MAS.Stuff.MapDistanceCalculatorService.Services;
using System.Threading.Tasks;

namespace MAS.Stuff.MapDistanceCalculatorService.Tests.Mocks
{
    public class AsyncWebMapServiceMock : IAsyncWebMapService
    {
        public async Task<string> GetDistance(string origin, string destination)
        {
            await Task.Factory.StartNew(() => { });
            return "{'OriginPoint': 'OriginPoint', 'DestinationPoint': 'DestinationPoint', 'Distance': 'Distance'}";
        }
    }
}
