﻿using MAS.Stuff.MapDistanceCalculatorService.Controllers;
using MAS.Stuff.MapDistanceCalculatorService.Models;
using MAS.Stuff.MapDistanceCalculatorService.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace MAS.Stuff.MapDistanceCalculatorService.Tests.Controllers
{
    [TestClass]
    public class DistanceControllerTest
    {
        private static DistanceController _distanceController;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            AsyncMapServiceMock mockMapService = new AsyncMapServiceMock();
            _distanceController = new DistanceController(mockMapService);
        }

        [TestMethod]
        public async Task GetByConcretePoint()
        {
            string origin = "origin",
                   destination = "destination";

            DistanceInfo response = await _distanceController.Get(origin, destination);

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task GetByCoordinates()
        {
            string x1 = 11.ToString(),
                   y1 = 21.ToString(),
                   x2 = 12.ToString(),
                   y2 = 22.ToString();

            DistanceInfo response = await _distanceController.Get(x1, y1, x2, y2);

            Assert.IsNotNull(response);
        }
    }
}
