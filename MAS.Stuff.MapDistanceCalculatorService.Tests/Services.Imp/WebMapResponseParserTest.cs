﻿using MAS.Stuff.MapDistanceCalculatorService.Models;
using MAS.Stuff.MapDistanceCalculatorService.Services.Imp;
using MAS.Stuff.MapDistanceCalculatorService.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MAS.Stuff.MapDistanceCalculatorService.Tests.Services.Imp
{
    [TestClass]
    public class GoogleResponseParserTest {
        [TestMethod]
        public void ParseTest()
        {
            GoogleResponseParser parser = new GoogleResponseParser();

            string unConvertedString = "{'OriginPoint': 'OriginPoint', 'DestinationPoint': 'DestinationPoint', 'Distance': 'Distance'}";

            DistanceInfo result = parser.Parse(unConvertedString);

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(DistanceInfo));
        }
    }
}
