﻿using MAS.Stuff.MapDistanceCalculatorService.Models;
using MAS.Stuff.MapDistanceCalculatorService.Services;
using MAS.Stuff.MapDistanceCalculatorService.Services.Imp;
using MAS.Stuff.MapDistanceCalculatorService.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace MAS.Stuff.MapDistanceCalculatorService.Tests.Services.Imp
{
    [TestClass]
    public class SimpleMapServiceTest
    {
        private static SimpleMapService _simpleMapService;

        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            IAsyncWebMapService asyncWebMapService = new AsyncWebMapServiceMock();
            IWebMapResponseParser webResponseParser = new WebMapResponseParserMock();

            _simpleMapService = new SimpleMapService(asyncWebMapService, webResponseParser);
        }
        [TestMethod]
        public async Task CalculateDistanceBetweenTwoPointsTest()
        {
            string origin = "origin",
                   destination = "destination";
            DistanceInfo response = await _simpleMapService.CalculateDistanceBetweenTwoPoints(origin, destination);

            Assert.IsNotNull(response);
        }
    }
}
