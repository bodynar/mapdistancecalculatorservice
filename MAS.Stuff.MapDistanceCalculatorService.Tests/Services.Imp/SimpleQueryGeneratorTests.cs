﻿using MAS.Stuff.MapDistanceCalculatorService.Models;
using MAS.Stuff.MapDistanceCalculatorService.Services.Imp;
using MAS.Stuff.MapDistanceCalculatorService.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MAS.Stuff.MapDistanceCalculatorService.Tests.Services.Imp {
    [TestClass]
    public class SimpleQueryGeneratorTests {
        [TestMethod]
        public void GenerateTest() {
            SimpleQueryBuilder queryGenerator = new SimpleQueryBuilder();

            string rootUrl = "http://ya.ru";

            IParamData[] parametrs =
            {
                new QueryParam("First", 0),
                new QueryParam("Second", 1),
                new QueryParam("Third", 2),
            };

            string resultedQuery = queryGenerator.Build(rootUrl, parametrs);

            Assert.IsNotNull(resultedQuery);
        }
    }
}
