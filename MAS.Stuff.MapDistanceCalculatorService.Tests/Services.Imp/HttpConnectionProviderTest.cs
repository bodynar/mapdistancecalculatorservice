﻿using MAS.Stuff.MapDistanceCalculatorService.Models;
using MAS.Stuff.MapDistanceCalculatorService.Services.Imp;
using MAS.Stuff.MapDistanceCalculatorService.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAS.Stuff.MapDistanceCalculatorService.Tests.Services.Imp {
    [TestClass]
    public class HttpConnectionProviderTest {
        [TestMethod]
        public async Task SendRequestAsyncTest() {
            HttpConnection httpProvider = new HttpConnection();

            string query = "https://ya.ru";

            string response = await httpProvider.SendRequestAsync(query);

            Assert.IsNotNull(response);
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectDisposedException))]
        public async Task DisposeTest() {
            HttpConnection httpProvider = new HttpConnection();

            string query = "https://ya.ru";

            httpProvider.Dispose();

            string response = await httpProvider.SendRequestAsync(query);
        }
    }
}
