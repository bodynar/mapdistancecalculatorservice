﻿namespace MAS.Stuff.MapDistanceCalculatorService.Services {
    #region References
    using MAS.Stuff.MapDistanceCalculatorService.Models;
    #endregion
    /// <summary>
    /// Генератор запроса к веб-сервису
    /// </summary>
    public interface IQueryBuilder {

        /// <summary>
        /// Формирование GET запроса-объекта к указанному адресу с набором параметров
        /// </summary>
        /// <param name="rootUrl">Адрес веб-сервиса</param>
        /// <param name="data">Параметры запроса</param>
        string Build(string rootUrl, params IParamData[] data); 
    }
}