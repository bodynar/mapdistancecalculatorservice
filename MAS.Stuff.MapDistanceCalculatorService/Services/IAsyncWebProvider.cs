﻿namespace MAS.Stuff.MapDistanceCalculatorService.Services
{
    #region References
    using MAS.Stuff.MapDistanceCalculatorService.Models;
    using System.Threading.Tasks;
    #endregion
    /// <summary>
    /// Провайдер подключения к удаленному веб-сервису
    /// </summary>
    public interface IAsyncWebProvider // todo: rename
    {
        /// <summary>
        /// Отправка GET запроса-объекта к веб-сервису
        /// </summary>
        /// <param name="query">Запрос-объект</param>
        /// <returns>Ответ веб-сервиса</returns>
        Task<string> SendRequestAsync(string query);
    }
}