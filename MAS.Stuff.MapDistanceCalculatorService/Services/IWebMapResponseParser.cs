﻿using System;
using MAS.Stuff.MapDistanceCalculatorService.Models;

namespace MAS.Stuff.MapDistanceCalculatorService.Services
{
    /// <summary>
    /// Парсер ответа от веб-сервиса
    /// </summary>
    public interface IWebMapResponseParser
    {
        /// <summary>
        /// Преобразование строкового ответа от веб-сервиса в модель данных
        /// </summary>
        /// <typeparam name="T">Тип модели данных</typeparam>
        /// <param name="response">Ответ от веб-сервиса</param>
        /// <returns>Ответ от веб-сервиса в виде модели данных</returns>
        DistanceInfo Parse(string response);
    }
}
