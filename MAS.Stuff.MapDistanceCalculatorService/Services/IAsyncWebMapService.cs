﻿namespace MAS.Stuff.MapDistanceCalculatorService.Services {
    #region References
    using System.Threading.Tasks;
    #endregion
    /// <summary>
    /// Провайдер работы с веб-сервисом карт
    /// </summary>
    public interface IAsyncWebMapService
    {
        /// <summary>
        /// Получение ответа от веб-сервиса с информацией о расстоянии
        /// </summary>
        /// <param name="origin">Стартовая точка</param>
        /// <param name="destination">Конечная точка</param>
        /// <returns></returns>
        Task<string> GetDistance(string origin, string destination);
    }
}
