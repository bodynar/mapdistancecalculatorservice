﻿namespace MAS.Stuff.MapDistanceCalculatorService.Services
{
    #region References
    using MAS.Stuff.MapDistanceCalculatorService.Models;
    using System.Threading.Tasks;
    #endregion
    /// <summary>
    /// Сервис работы с картами
    /// </summary>
    public interface IAsyncMapService {
        /// <summary>
        /// Расчет расстояния между двумя точками
        /// </summary>
        /// <param name="origin">Исходная точка</param>
        /// <param name="destination">Точка назначения</param>
        /// <returns>Информация о расстоянии</returns>
        Task<DistanceInfo> CalculateDistanceBetweenTwoPoints(string origin, string destination);

        /// <summary>
        /// Расчет расстояния между двумя точками (по их координатам)
        /// </summary>
        /// <param name="x1">Широта первой точки</param>
        /// <param name="y1">Долгота первой точки</param>
        /// <param name="x2">Широта второй точки</param>
        /// <param name="y2">Долгота второй точки</param>
        /// <returns></returns>
        Task<DistanceInfo> CalculateDistanceBetweenTwoPoints(string x1, string y1, string x2, string y2);
    }
}
