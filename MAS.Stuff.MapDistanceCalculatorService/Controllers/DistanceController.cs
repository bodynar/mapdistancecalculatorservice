﻿using System;

namespace MAS.Stuff.MapDistanceCalculatorService.Controllers
{
    #region References

    using System.Web.Http;
    using Services;
    using System.Threading.Tasks;
    using Models;
    using Microsoft.Practices.Unity;

    #endregion

    public class DistanceController : ApiController
    {
        private readonly IAsyncMapService _mapService;
        //private readonly IUnityContainer _container;

        public DistanceController(IAsyncMapService mapService/*, IUnityContainer container = null*/)
        {
            //_container = container;
            _mapService = mapService;
        }

        [HttpGet]
        public Task<DistanceInfo> Get(string origin, string destination)
        {
            var response = _mapService.CalculateDistanceBetweenTwoPoints(origin, destination);
            return response;
        }

        [HttpGet]
        public Task<DistanceInfo> Get(string x1, string y1, string x2, string y2)
        {
            throw new NotImplementedException();
            var response = _mapService.CalculateDistanceBetweenTwoPoints(x1, y1, x2, y2);
            return response;
        }
    }
}