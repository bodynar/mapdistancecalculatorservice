﻿using System.Web.Configuration;
using MAS.Stuff.MapDistanceCalculatorService.Controllers;
using MAS.Stuff.MapDistanceCalculatorService.Services;
using MAS.Stuff.MapDistanceCalculatorService.Services.Imp;
using Microsoft.Practices.Unity;

namespace MAS.Stuff.MapDistanceCalculatorService.DI {
    public static class UnityContainerConfiguration
    {
        public static IUnityContainer Initialize()
        {
            var container = new UnityContainer();

            container.RegisterType<IQueryBuilder, SimpleQueryBuilder>();
            container.RegisterType<IAsyncWebMapService, GoogleService>();
            container.RegisterType<IAsyncMapService, SimpleMapService>();
            container.RegisterType<IAsyncWebProvider, HttpConnection>();
            container.RegisterType<IWebMapResponseParser, GoogleResponseParser>();

            var webParser = container.Resolve<IWebMapResponseParser>();
            var webProvider = container.Resolve<IAsyncWebProvider>();
            var queryGenerator = container.Resolve<IQueryBuilder>();

            var googleApiKey = WebConfigurationManager.AppSettings["ApiKey:Google"]; // это нормально?

            container.RegisterType<GoogleService>(new InjectionFactory((_container) => new GoogleService(googleApiKey, queryGenerator, webProvider)));

            var webMap = container.Resolve<IAsyncWebMapService>();

            container.RegisterType<SimpleMapService>(new InjectionFactory((_container) => new SimpleMapService(webMap, webParser)));

            var mapService = container.Resolve<IAsyncMapService>();

            container.RegisterType<DistanceController>(new InjectionFactory((_container) => new DistanceController(mapService)));

            return container;
        }
    }
}