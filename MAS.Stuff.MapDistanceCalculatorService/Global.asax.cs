﻿namespace MAS.Stuff.MapDistanceCalculatorService {
    #region References
    using System.Web.Http;
    using System.Web.Mvc;
    #endregion
    public class WebApiApplication : System.Web.HttpApplication {
        protected void Application_Start() {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        }
    }
}
