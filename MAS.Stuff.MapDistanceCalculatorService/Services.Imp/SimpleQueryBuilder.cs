﻿namespace MAS.Stuff.MapDistanceCalculatorService.Services.Imp {
    #region References
    using MAS.Stuff.MapDistanceCalculatorService.Models;
    using System;
    #endregion
    public class SimpleQueryBuilder : IQueryBuilder {
        public string Build(string rootUrl, params IParamData[] data)
        {
            string query = string.Format($"{rootUrl}?");

            foreach (var param in data)
                query += string.Format($"{param.Name}={param.Value}&");

            return query;
        }
    }
}