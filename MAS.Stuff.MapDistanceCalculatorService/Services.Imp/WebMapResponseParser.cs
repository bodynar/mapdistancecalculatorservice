﻿namespace MAS.Stuff.MapDistanceCalculatorService.Services.Imp {
    #region References
    using System;
    using System.Web.Helpers;
    using MAS.Stuff.MapDistanceCalculatorService.Models;
    #endregion

    public class GoogleResponseParser : IWebMapResponseParser {
        public DistanceInfo Parse(string response) {
            var jsonObject = Json.Decode(response);

            var status = (string)jsonObject["status"];

            if (status != "OK") // hmmmmm
                return null;

            var destination = (string)jsonObject["destination_addresses"][0];
            var origin = (string)jsonObject["origin_addresses"][0];

            status = (string)jsonObject["rows"][0]["elements"][0]["status"];

            if (status != "OK") // hmmmmm
                return null;

            var distance = jsonObject["rows"][0]["elements"][0]["distance"]["value"];
            var duration = jsonObject["rows"][0]["elements"][0]["duration"]["value"];

            var distanceInfo = new DistanceInfo(origin, destination, distance, duration);

            return distanceInfo;
        }

    }
}