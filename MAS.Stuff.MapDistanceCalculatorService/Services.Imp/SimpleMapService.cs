﻿namespace MAS.Stuff.MapDistanceCalculatorService.Services.Imp {
    #region References
    using MAS.Stuff.MapDistanceCalculatorService.Models;
    using System.Threading.Tasks;
    #endregion

    public class SimpleMapService : IAsyncMapService {
        private readonly IAsyncWebMapService _webMapProvider;
        private readonly IWebMapResponseParser _webMapResponseParser;

        public SimpleMapService(
            IAsyncWebMapService webMapProvider,
            IWebMapResponseParser webMapResponseParser) {
            _webMapProvider = webMapProvider;
            _webMapResponseParser = webMapResponseParser;
        }

        public async Task<DistanceInfo> CalculateDistanceBetweenTwoPoints(string origin, string destination) {
            var response = _webMapProvider.GetDistance(origin, destination);

            return _webMapResponseParser.Parse(await response);
        }

        public Task<DistanceInfo> CalculateDistanceBetweenTwoPoints(string x1, string y1, string x2, string y2) {
            string origin = string.Format($"{x1},{y1}"),
                destination = string.Format($"{x2},{y2}");

            return CalculateDistanceBetweenTwoPoints(origin, destination);
        }
    }
}