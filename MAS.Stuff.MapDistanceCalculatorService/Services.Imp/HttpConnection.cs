﻿namespace MAS.Stuff.MapDistanceCalculatorService.Services.Imp
{
    #region References
    using System;
    using System.Threading.Tasks;
    using MAS.Stuff.MapDistanceCalculatorService.Models;
    using System.Net;
    using System.Text;
    #endregion
    public class HttpConnection : IAsyncWebProvider, IDisposable // todo: rename
    {
        private WebClient _client;
        public HttpConnection()
        {
            _client = new WebClient();
        }

        public async Task<string> SendRequestAsync(string query)
        {
            if (_client == null)
                throw new ObjectDisposedException("Connection was disposed");

            var response = _client.DownloadDataTaskAsync(query);

            return Encoding.UTF8.GetString(await response);
        }

        #region IDisposable implementation

        public void Dispose()
        {
            if (_client != null)
            {
                _client?.Dispose();
                _client = null;
            }
        }

        #endregion
    }
}