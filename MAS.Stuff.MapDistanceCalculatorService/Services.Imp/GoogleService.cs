﻿namespace MAS.Stuff.MapDistanceCalculatorService.Services.Imp {
    #region References
    using Models;
    using System.Threading.Tasks;
    #endregion
    public class GoogleService : IAsyncWebMapService {
        private readonly IQueryBuilder _queryGenerator;
        private readonly IAsyncWebProvider _webProvider;
        private readonly string _apiKey;
        private const string GoogleApiUrl = "https://maps.googleapis.com/maps/api/distancematrix/json";

        // example
        /*
        ?origins=64.535161,%2040.551114&destinations=64.527185,%2040.596192&key=AIzaSyCFCzfeRB8o155NyLLD_6CPE63zS8d_n9w
        */

        public GoogleService(string apiKey, IQueryBuilder queryGenerator, IAsyncWebProvider webProvider) {
            _apiKey = apiKey;
            _queryGenerator = queryGenerator;
            _webProvider = webProvider;
        }

        public Task<string> GetDistance(string origins, string destinations) {

            QueryParam firstPointParam = new QueryParam(nameof(origins), origins),
                       secondPointParam = new QueryParam(nameof(destinations), destinations),
                       apiParam = new QueryParam("key", _apiKey);

            var query = _queryGenerator.Build(GoogleApiUrl, firstPointParam, secondPointParam, apiParam);

            return _webProvider.SendRequestAsync(query);
        }
    }
}