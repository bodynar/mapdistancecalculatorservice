﻿namespace MAS.Stuff.MapDistanceCalculatorService.Models {
    /// <summary>
    /// Параметр запроса
    /// </summary>
    public interface IParamData {
        /// <summary>
        /// Название параметра
        /// </summary>
        string Name { get; }
       
        /// <summary>
        /// Значение параметра
        /// </summary>
        object Value { get;  }
    }
}