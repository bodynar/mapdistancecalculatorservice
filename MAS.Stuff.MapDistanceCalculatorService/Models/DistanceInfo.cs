﻿namespace MAS.Stuff.MapDistanceCalculatorService.Models
{
    /// <summary>
    /// Информация о расстоянии между двумя точками
    /// </summary>
    public class DistanceInfo 
    {
        public DistanceInfo(string originPoint, string destinationPoint, int distance, int routeTime)
        {
            OriginPoint = originPoint;
            DestinationPoint = destinationPoint;
            Distance = distance;
            RouteTime = routeTime;
        }

        /// <summary>
        /// Исходная точка
        /// </summary>
        public string OriginPoint { get;  }

        /// <summary>
        /// Конечная точка
        /// </summary>
        public string DestinationPoint { get;  }

        /// <summary>
        /// Расстояние (в метрах)
        /// </summary>
        public int Distance { get;  }

        /// <summary>
        /// Время в пути (в минутах)
        /// </summary>
        public int RouteTime { get; }
    }
}