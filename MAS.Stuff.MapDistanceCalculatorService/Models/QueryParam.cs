﻿namespace MAS.Stuff.MapDistanceCalculatorService.Models {
    public class QueryParam : IParamData {
        public string Name { get; }

        public object Value { get; }

        public QueryParam(string name, object value)
        {
            Name = name;
            Value = value;
        }
    }
}