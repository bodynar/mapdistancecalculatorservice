﻿using System.Web;
using System.Web.Mvc;

namespace MAS.Stuff.MapDistanceCalculatorService {
    public class FilterConfig {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
