﻿using MAS.Stuff.MapDistanceCalculatorService.DI;
using System.Web.Http;

namespace MAS.Stuff.MapDistanceCalculatorService
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Конфигурация и службы веб-API

            // Маршруты веб-API
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var container = UnityContainerConfiguration.Initialize();
            config.DependencyResolver = new UnityResolver(container);
        }
    }
}
